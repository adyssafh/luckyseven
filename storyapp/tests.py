

# Create your tests here.
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import landingpage

class Lab7UnitTest(TestCase):
    def test_lab7_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_landing_func(self):
        found = resolve('/')
        self.assertEqual(found.func, page)

    def test_lab7_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingpage.html')

