from django.urls import path
from . import views

app_name = 'storyapp'

urlpatterns = [
    path('', views.landingpage, name='landingpage'),
    path('blog/', views.blog, name='blog'),
    # dilanjutkan ...
]